"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const DiceRollService_1 = require("../../util/DiceRollService");
module.exports = class DnDChargenCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            aliases: ['dnd-chargen', 'generate-character'],
            description: 'Create a stat spread for a D&D 5e character',
            group: 'dice',
            memberName: 'chargen',
            name: 'chargen'
        });
    }
    run(message) {
        let statsMsg = '';
        for (const stat of ['Strength', 'Dexterity', 'Constitution', 'Intelligence', 'Wisdom', 'Charisma']) {
            const { prettyResults, total } = DiceRollService_1.DiceRollService.keepHighest(3, DiceRollService_1.DiceRollService.rollDice(4, 6).rolls);
            statsMsg += `${stat}: [${prettyResults.join(', ')}] = ${total}\n`;
        }
        return message.say(statsMsg);
    }
};
