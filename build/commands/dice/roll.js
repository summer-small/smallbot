"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const DiceRollService_1 = require("../../util/DiceRollService");
module.exports = class RollCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            aliases: ['r', 'dice'],
            args: [
                {
                    key: 'rollString',
                    prompt: 'Roll dice, e.g. 1d4',
                    type: 'string'
                }
            ],
            description: 'Rolls dice!',
            group: 'dice',
            memberName: 'roll',
            name: 'roll'
        });
    }
    run(message, { rollString }) {
        var _a;
        const mention = ((_a = message.member) === null || _a === void 0 ? void 0 : _a.nickname) || message.author.username;
        const roller = new DiceRollService_1.DiceRollService(rollString);
        if (roller.errors.length) {
            return message.say(`${mention} ERROR: ${roller.errors.join(', ')}`);
        }
        const { prettyResults, total } = roller.roll();
        return message.say(`${mention}: [${prettyResults.join(', ')}] = ${total}`);
    }
};
