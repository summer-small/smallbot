"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
module.exports = class PauseCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            description: 'Pause music',
            group: 'audio',
            guildOnly: true,
            memberName: 'pause',
            name: 'pause'
        });
    }
    run(message) {
        const guild = message.guild;
        if (!guild.isPlaying) {
            return message.say('Not playing any music right now');
        }
        guild.pause();
        return null;
    }
};
