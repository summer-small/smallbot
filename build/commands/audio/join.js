"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
module.exports = class JoinCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            clientPermissions: ['CONNECT'],
            description: 'Join the current voice channel',
            group: 'audio',
            guildOnly: true,
            memberName: 'join',
            name: 'join'
        });
    }
    run(message) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if (!((_a = message.member) === null || _a === void 0 ? void 0 : _a.voice.channel)) {
                return message.say('Join a voice channel first');
            }
            yield message.guild.connect(message.member.voice.channel);
            return null;
        });
    }
};
