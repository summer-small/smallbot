"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
module.exports = class QueueCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            aliases: ['list'],
            description: 'List the current music queue',
            group: 'audio',
            guildOnly: true,
            memberName: 'queue',
            name: 'queue'
        });
    }
    run(message) {
        const currentSong = message.guild.currentSong;
        const queue = message.guild.queue;
        if (!queue.length && !currentSong) {
            return message.say('Queue empty. Play something with "!play"');
        }
        const playing = currentSong ? `Playing - ${currentSong.name}\n` : '';
        return message.say(`${playing}${queue.map(x => x.name).join('\n')}`);
    }
};
