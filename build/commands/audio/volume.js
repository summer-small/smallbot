"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const regexes_1 = require("../../util/regexes");
module.exports = class VolumeCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            args: [{
                    default: '',
                    key: 'command',
                    prompt: 'Volume is...',
                    type: 'string'
                }],
            description: 'Set the volume',
            group: 'audio',
            guildOnly: true,
            memberName: 'volume',
            name: 'volume'
        });
    }
    run(message, { command }) {
        const guild = message.guild;
        if (!command) {
            return message.say(`Volume: ${Math.floor(guild.volume * 100)}%`);
        }
        const match = command.match(regexes_1.VOLUME_REGEX);
        if (!match) {
            return message.say('Invalid volume message');
        }
        let op;
        if (match[1]) {
            op = match[1] === '+' ? 'add' : 'subtract';
        }
        else {
            op = 'set';
        }
        const v = parseInt(match[2], 10) / 100;
        const newV = guild.setVolume(v, op);
        return message.say(`Volume: ${Math.floor(newV * 100)}%`);
    }
};
