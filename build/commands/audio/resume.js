"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
module.exports = class ResumeCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            description: 'Resume music',
            group: 'audio',
            guildOnly: true,
            memberName: 'resume',
            name: 'resume'
        });
    }
    run(message) {
        const guild = message.guild;
        if (guild.isPlaying) {
            return message.say('Already playing music');
        }
        if (!guild.dispatcher) {
            return message.say('No music to resume');
        }
        guild.resume();
        return null;
    }
};
