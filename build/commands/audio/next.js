"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
module.exports = class NextCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            aliases: ['skip'],
            description: 'Skip to the next song',
            group: 'audio',
            guildOnly: true,
            memberName: 'next',
            name: 'next'
        });
    }
    run(message) {
        message.guild.next();
        return null;
    }
};
