"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const googleapis_1 = require("googleapis");
const regexes_1 = require("../../util/regexes");
const youtube = googleapis_1.google.youtube({
    auth: process.env.google_api_key,
    version: 'v3'
});
module.exports = class PlayCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            args: [
                {
                    key: 'query',
                    prompt: 'What song would you like to listen to?',
                    type: 'string'
                }
            ],
            clientPermissions: ['SPEAK', 'CONNECT'],
            description: 'Play somthing!',
            group: 'audio',
            guildOnly: true,
            memberName: 'play',
            name: 'play'
        });
    }
    run(message, { query }) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
        return __awaiter(this, void 0, void 0, function* () {
            if (!((_a = message.member) === null || _a === void 0 ? void 0 : _a.voice.channel)) {
                return message.say('Join a voice channel first');
            }
            const guild = message.guild;
            if (!guild.connection) {
                yield guild.connect(message.member.voice.channel);
            }
            if (query.match(regexes_1.YOUTUBE_PLAYLIST_REGEX)) {
                const playlistId = query.match(regexes_1.YOUTUBE_PLAYLIST_REGEX)[1];
                const res = yield youtube.playlistItems.list({
                    part: ['contentDetails', 'snippet'],
                    playlistId
                });
                if (!((_b = res.data.items) === null || _b === void 0 ? void 0 : _b.length)) {
                    return message.say('Cannot find an playlist matching that url');
                }
                const videos = res.data.items.map(x => {
                    var _a, _b, _c, _d, _e;
                    return {
                        name: (_a = x.snippet) === null || _a === void 0 ? void 0 : _a.title,
                        play: `https://www.youtube.com/watch?v=${(_b = x.contentDetails) === null || _b === void 0 ? void 0 : _b.videoId}`,
                        thumbnail: (_e = (_d = (_c = x.snippet) === null || _c === void 0 ? void 0 : _c.thumbnails) === null || _d === void 0 ? void 0 : _d.standard) === null || _e === void 0 ? void 0 : _e.url
                    };
                });
                guild.play(message, ...videos);
            }
            else if (query.match(regexes_1.YOUTUBE_VIDEO_REGEX)) {
                const videoId = query.match(regexes_1.YOUTUBE_VIDEO_REGEX)[8];
                const res = yield youtube.videos.list({
                    id: [videoId],
                    part: ['id', 'snippet']
                });
                if (!((_c = res.data.items) === null || _c === void 0 ? void 0 : _c.length)) {
                    return message.say('Cannot find that video');
                }
                guild.play(message, {
                    name: (_d = res.data.items[0].snippet) === null || _d === void 0 ? void 0 : _d.title,
                    play: `https://www.youtube.com/watch?v=${res.data.items[0].id}`,
                    thumbnail: (_g = (_f = (_e = res.data.items[0].snippet) === null || _e === void 0 ? void 0 : _e.thumbnails) === null || _f === void 0 ? void 0 : _f.standard) === null || _g === void 0 ? void 0 : _g.url
                });
            }
            else {
                const res = yield youtube.search.list({
                    part: ['id', 'snippet'],
                    q: query
                });
                if (!((_h = res.data.items) === null || _h === void 0 ? void 0 : _h.length)) {
                    return message.say('Cannot find any videos matching that query');
                }
                guild.play(message, {
                    name: (_j = res.data.items[0].snippet) === null || _j === void 0 ? void 0 : _j.title,
                    play: `https://www.youtube.com/watch?v=${res.data.items[0].id.videoId}`,
                    thumbnail: (_m = (_l = (_k = res.data.items[0].snippet) === null || _k === void 0 ? void 0 : _k.thumbnails) === null || _l === void 0 ? void 0 : _l.standard) === null || _m === void 0 ? void 0 : _m.url
                });
            }
            return null;
        });
    }
};
