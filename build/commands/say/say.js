"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
module.exports = class SayCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            aliases: ['echo', 'speak'],
            args: [
                {
                    key: 'text',
                    prompt: 'What should I say?',
                    type: 'string'
                }
            ],
            description: 'Make me say something!',
            group: 'say',
            memberName: 'say',
            name: 'say'
        });
    }
    run(message, { text }) {
        return message.say(text);
    }
};
