"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AudioGuild = void 0;
const discord_js_commando_1 = require("discord.js-commando");
const ytdl_core_discord_1 = __importDefault(require("ytdl-core-discord"));
class AudioGuild extends discord_js_commando_1.CommandoGuild {
    constructor(commandoClient, data) {
        super(commandoClient, data);
        this.isPlaying = false;
        this.queue = [];
        this.connection = null;
        this.currentSong = null;
        this.dispatcher = null;
        this.isPlaying = false;
        this.message = null;
        this.queue = [];
        this.volume = 1;
    }
    connect(channel) {
        return __awaiter(this, void 0, void 0, function* () {
            this.connection = yield channel.join();
        });
    }
    next() {
        var _a;
        (_a = this.dispatcher) === null || _a === void 0 ? void 0 : _a.end();
    }
    pause() {
        var _a;
        this.isPlaying = false;
        (_a = this.dispatcher) === null || _a === void 0 ? void 0 : _a.pause();
    }
    play(message, ...songs) {
        this.message = message;
        if (!this.connection) {
            console.log('No connection');
            return;
        }
        this.queue.push(...songs);
        if (!this.isPlaying && !this.dispatcher) {
            this.playSong(this.queue[0].play);
        }
    }
    resume() {
        var _a;
        this.isPlaying = true;
        (_a = this.dispatcher) === null || _a === void 0 ? void 0 : _a.resume();
    }
    setVolume(v, op = 'set') {
        var _a;
        const currentVolume = this.volume;
        if (op === 'set') {
            this.volume = v;
        }
        else if (op === 'add') {
            this.volume = currentVolume + v;
        }
        else {
            this.volume = currentVolume - v;
        }
        this.volume = Math.max(Math.min(this.volume, 1), 0);
        (_a = this.dispatcher) === null || _a === void 0 ? void 0 : _a.setVolume(v);
        return this.volume;
    }
    playSong(song) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.connection) {
                return;
            }
            let play;
            if (typeof song === 'string') {
                play = yield ytdl_core_discord_1.default(song);
            }
            else {
                play = song;
            }
            this.dispatcher = this.connection.play(play, { type: 'opus' })
                .on('start', () => {
                var _a, _b;
                this.currentSong = Object.assign({}, this.queue[0]);
                this.isPlaying = true;
                (_a = this.dispatcher) === null || _a === void 0 ? void 0 : _a.setVolume(this.volume);
                (_b = this.message) === null || _b === void 0 ? void 0 : _b.say(`Playing ${this.queue[0].name}`);
                return this.queue.shift();
            })
                .on('finish', () => {
                if (!this.queue.length) {
                    this.currentSong = null;
                    this.isPlaying = false;
                    this.dispatcher = null;
                    return;
                }
                this.playSong(this.queue[0].play);
            })
                .on('error', err => {
                var _a;
                console.error(err);
                (_a = this.message) === null || _a === void 0 ? void 0 : _a.say('An error has occurred! Can\'t play that song');
            });
        });
    }
}
exports.AudioGuild = AudioGuild;
