"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiceRollService = void 0;
const regexes_1 = require("./regexes");
class DiceRollService {
    constructor(rollArg) {
        this.commands = [];
        this.comment = '';
        this.errors = [];
        this.result = 0;
        const commentIdx = rollArg.indexOf('#');
        let roll;
        if (commentIdx >= 0) {
            roll = rollArg.slice(0, commentIdx);
            this.comment = rollArg.slice(commentIdx + 1).trim();
        }
        else {
            roll = rollArg;
        }
        this.rollArg = roll.toLowerCase().replace(/\s+/g, '');
        this.parseWholeRoll();
        this.validateCommands();
    }
    roll() {
        const runningResult = {
            prettyResults: [],
            rolls: [],
            total: 0
        };
        let nextCommand = '+';
        for (const command of this.commands) {
            if (command.match(regexes_1.OPERATOR_REGEX)) {
                nextCommand = command;
            }
            const match = command.match(regexes_1.ROLL_REGEX);
            if (!match) {
                continue;
            }
            const rollResult = {
                prettyResults: [],
                rolls: [],
                total: 0
            };
            const isRoll = match[2] === 'd';
            const nDie = parseInt(match[1], 10) || 1;
            const dieSize = parseInt(match[3], 10);
            const keepExtemes = match[4];
            if (!isRoll) {
                rollResult.prettyResults = [dieSize.toString()];
                rollResult.rolls = [dieSize];
                rollResult.total = dieSize;
            }
            else {
                const rollRes = DiceRollService.rollDice(nDie, dieSize);
                if (keepExtemes) {
                    let keepFn;
                    if (keepExtemes === 'a') {
                        keepFn = DiceRollService.keepHighest;
                    }
                    else if (keepExtemes === 'd') {
                        keepFn = DiceRollService.keepLowest;
                    }
                    else {
                        keepFn = match[4][1] === 'l' ? DiceRollService.keepLowest : DiceRollService.keepHighest;
                    }
                    const keepRes = keepFn(parseInt(match[4][2], 10) || 1, rollRes.rolls);
                    rollResult.prettyResults = keepRes.prettyResults;
                    rollResult.rolls = keepRes.rolls;
                    rollResult.total = keepRes.total;
                }
                else {
                    rollResult.prettyResults = rollRes.prettyResults;
                    rollResult.rolls = rollRes.rolls;
                    rollResult.total = rollRes.total;
                }
            }
            runningResult.prettyResults.push(...rollResult.prettyResults);
            runningResult.rolls.push(...rollResult.rolls);
            runningResult.total = this.applyOp(runningResult.total, rollResult.total, nextCommand);
        }
        return runningResult;
    }
    static rollDice(nDie, dieSize) {
        const rolls = [];
        let total = 0;
        for (let i = 0; i < nDie; i++) {
            const roll = rollSingle(dieSize);
            rolls.push(roll);
            total += roll;
        }
        return {
            rolls,
            prettyResults: rolls.map(x => x.toString()),
            total
        };
    }
    static keepHighest(nToKeep, rolls) {
        return keepExtreme('highest', nToKeep, rolls);
    }
    static keepLowest(nToKeep, rolls) {
        return keepExtreme('lowest', nToKeep, rolls);
    }
    applyOp(a, b, op) {
        let res;
        switch (op) {
            case '+':
                res = a + b;
                break;
            case '-':
                res = a - b;
                break;
            case '*':
                res = a * b;
                break;
            case '/':
                res = a / b;
        }
        return res;
    }
    parseWholeRoll() {
        let lastOpIdx = 0;
        for (let i = 0; i < this.rollArg.length; i++) {
            const char = this.rollArg.charAt(i);
            if (char.match(regexes_1.OPERATOR_REGEX)) {
                this.commands.push(this.rollArg.substring(lastOpIdx, i));
                this.commands.push(this.rollArg.substr(i, 1));
                lastOpIdx = i + 1;
            }
        }
        this.commands.push(this.rollArg.substr(lastOpIdx));
    }
    validateCommands() {
        if (this.commands[0].match(regexes_1.OPERATOR_REGEX)) {
            this.errors.push(`Cannot start roll with ${this.commands[0]}`);
        }
        for (const command of this.commands) {
            if (!command.match(regexes_1.OPERATOR_REGEX) && !command.match(regexes_1.ROLL_REGEX)) {
                this.errors.push(`${command} is an invalid roll`);
            }
        }
    }
}
exports.DiceRollService = DiceRollService;
function rollSingle(dieSize, includeZero = false) {
    return Math.floor(Math.random() * dieSize) + (includeZero ? 0 : 1);
}
function keepExtreme(highOrLow, nToKeep, rolls) {
    const sortedRolls = [...rolls].sort((a, b) => a - b);
    const keptRolls = highOrLow === 'highest' ? sortedRolls.slice(-nToKeep) : sortedRolls.slice(0, nToKeep);
    const prettyResults = rolls.map(x => x.toString());
    let total = 0;
    for (const roll of keptRolls) {
        total += roll;
        const idx = prettyResults.indexOf(roll.toString());
        if (idx >= 0) {
            prettyResults[idx] = `---${prettyResults[idx]}`;
        }
    }
    return {
        prettyResults: prettyResults.map(x => {
            if (x.match(/^---/)) {
                return x.slice(3);
            }
            return `~~${x}~~`;
        }),
        rolls: keptRolls,
        total
    };
}
