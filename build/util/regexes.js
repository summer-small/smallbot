"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YOUTUBE_VIDEO_REGEX = exports.YOUTUBE_PLAYLIST_REGEX = exports.ROLL_REGEX = exports.OPERATOR_REGEX = exports.VOLUME_REGEX = void 0;
// Audio Regexes
exports.VOLUME_REGEX = new RegExp(/^([+\-])?([0-9]+)%?$/);
// Dice Regexes
exports.OPERATOR_REGEX = new RegExp(/[+\-*/]/);
exports.ROLL_REGEX = new RegExp(/^([0-9]*)(d?)([0-9]+)((k?[hl]?[0-9]+)|[ad])?$/);
// Youtube Regexes
exports.YOUTUBE_PLAYLIST_REGEX = new RegExp(/^(?!.*\?.*\bv=)https:\/\/www\.youtube\.com\/.*\?.*\blist=(.*)$/);
exports.YOUTUBE_VIDEO_REGEX = new RegExp(/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/(watch\?v=)?([^\?&\s]+).*$/);
