import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {DiceRollService} from '../../util/DiceRollService';

module.exports = class DnDChargenCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      aliases: ['dnd-chargen', 'generate-character'],
      description: 'Create a stat spread for a D&D 5e character',
      group: 'dice',
      memberName: 'chargen',
      name: 'chargen'
    });
  }

  public run(message: CommandoMessage) {
    let statsMsg: string = '';
    for (const stat of ['Strength', 'Dexterity', 'Constitution', 'Intelligence', 'Wisdom', 'Charisma']) {
      const {prettyResults, total} = DiceRollService.keepHighest(3, DiceRollService.rollDice(4, 6).rolls);

      statsMsg += `${stat}: [${prettyResults.join(', ')}] = ${total}\n`;
    }

    return message.say(statsMsg);
  }
};