import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {DiceRollService} from '../../util/DiceRollService';

module.exports = class RollCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      aliases: ['r', 'dice'],
      args: [
        {
          key: 'rollString',
          prompt: 'Roll dice, e.g. 1d4',
          type: 'string'
        }
      ],
      description: 'Rolls dice!',
      group: 'dice',
      memberName: 'roll',
      name: 'roll'
    });
  }

  public run(message: CommandoMessage, {rollString}: {rollString: string}) {
    const mention = message.member?.nickname || message.author.username;
    const roller = new DiceRollService(rollString);

    if (roller.errors.length) {
      return message.say(`${mention} ERROR: ${roller.errors.join(', ')}`);
    }

    const {prettyResults, total} = roller.roll();

    return message.say(`${mention}: [${prettyResults.join(', ')}] = ${total}`);
  }
};