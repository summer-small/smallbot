import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';

module.exports = class ResumeCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      description: 'Resume music',
      group: 'audio',
      guildOnly: true,
      memberName: 'resume',
      name: 'resume'
    });
  }

  public run(message: CommandoMessage) {
    const guild = message.guild as AudioGuild;

    if (guild.isPlaying) {
      return message.say('Already playing music');
    }

    if (!guild.dispatcher) {
      return message.say('No music to resume');
    }

    guild.resume();

    return null;
  }
};