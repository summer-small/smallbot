import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';
import {VOLUME_REGEX} from '../../util/regexes';

export type VolumeOperation = 'set' | 'add' | 'subtract';

module.exports = class VolumeCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      args: [{
        default: '',
        key: 'command',
        prompt: 'Volume is...',
        type: 'string'
      }],
      description: 'Set the volume',
      group: 'audio',
      guildOnly: true,
      memberName: 'volume',
      name: 'volume'
    });
  }

  public run(message: CommandoMessage, {command}: {command: string}) {
    const guild = message.guild as AudioGuild;
    if (!command) {
      return message.say(`Volume: ${Math.floor(guild.volume * 100)}%`);
    }

    const match = command.match(VOLUME_REGEX);
    if (!match) {
      return message.say('Invalid volume message');
    }

    let op: VolumeOperation;
    if (match[1]) {
      op = match[1] === '+' ? 'add' : 'subtract';
    } else {
      op = 'set';
    }

    const v = parseInt(match[2], 10) / 100;
    const newV = guild.setVolume(v, op);

    return message.say(`Volume: ${Math.floor(newV * 100)}%`);
  }
};