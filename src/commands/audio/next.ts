import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';

module.exports = class NextCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      aliases: ['skip'],
      description: 'Skip to the next song',
      group: 'audio',
      guildOnly: true,
      memberName: 'next',
      name: 'next'
    });
  }

  public run(message: CommandoMessage) {
    (message.guild as AudioGuild).next();

    return null;
  }
};