import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';

module.exports = class DisconnectCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      aliases: ['leave', 'stop'],
      clientPermissions: ['CONNECT'],
      description: 'Leave the current voice channel',
      group: 'audio',
      guildOnly: true,
      memberName: 'disconnect',
      name: 'disconnect'
    });
  }

  public async run(message: CommandoMessage) {
    (message.guild as AudioGuild).connection?.disconnect();

    return null;
  }
};