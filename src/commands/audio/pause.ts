import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';

module.exports = class PauseCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      description: 'Pause music',
      group: 'audio',
      guildOnly: true,
      memberName: 'pause',
      name: 'pause'
    });
  }

  public run(message: CommandoMessage) {
    const guild = message.guild as AudioGuild;

    if (!guild.isPlaying) {
      return message.say('Not playing any music right now');
    }

    guild.pause();

    return null;
  }
};