import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';

module.exports = class QueueCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      aliases: ['list'],
      description: 'List the current music queue',
      group: 'audio',
      guildOnly: true,
      memberName: 'queue',
      name: 'queue'
    });
  }

  public run(message: CommandoMessage) {
    const currentSong = (message.guild as AudioGuild).currentSong;
    const queue = (message.guild as AudioGuild).queue;

    if (!queue.length && !currentSong) {
      return message.say('Queue empty. Play something with "!play"');
    }

    const playing = currentSong ? `Playing - ${currentSong.name}\n` : '';

    return message.say(`${playing}${queue.map(x => x.name).join('\n')}`);
  }
};