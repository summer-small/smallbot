import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {google} from 'googleapis';
import {AudioGuild} from '../../util/AudioGuild';
import {YOUTUBE_PLAYLIST_REGEX, YOUTUBE_VIDEO_REGEX} from '../../util/regexes';

const youtube = google.youtube({
  auth: process.env.google_api_key,
  version: 'v3'
});

module.exports = class PlayCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      args: [
        {
          key: 'query',
          prompt: 'What song would you like to listen to?',
          type: 'string'
        }
      ],
      clientPermissions: ['SPEAK', 'CONNECT'],
      description: 'Play somthing!',
      group: 'audio',
      guildOnly: true,
      memberName: 'play',
      name: 'play'
    });
  }

  public async run(message: CommandoMessage, {query}: {query: string}) {
	  if (!message.member?.voice.channel) {
      return message.say('Join a voice channel first');
    }

    const guild = message.guild as AudioGuild;

    if (!guild.connection) {
      await guild.connect(message.member.voice.channel);
    }

    if (query.match(YOUTUBE_PLAYLIST_REGEX)) {
      const playlistId = query.match(YOUTUBE_PLAYLIST_REGEX)![1];
      const res = await youtube.playlistItems.list({
        part: ['contentDetails', 'snippet'],
        playlistId
      });

      if (!res.data.items?.length) {
        return message.say('Cannot find an playlist matching that url');
      }

      const videos = res.data.items.map(x => {
        return {
          name: x.snippet?.title!,
          play: `https://www.youtube.com/watch?v=${x.contentDetails?.videoId}`,
          thumbnail: x.snippet?.thumbnails?.standard?.url!
        };
      });
      guild.play(message, ...videos);
    } else if (query.match(YOUTUBE_VIDEO_REGEX)) {
      const videoId = query.match(YOUTUBE_VIDEO_REGEX)![8];
      const res = await youtube.videos.list({
        id: [videoId],
        part: ['id', 'snippet']
      });

      if (!res.data.items?.length) {
        return message.say('Cannot find that video');
      }

      guild.play(message, {
        name: res.data.items[0].snippet?.title!,
        play: `https://www.youtube.com/watch?v=${res.data.items[0].id}`,
        thumbnail: res.data.items[0].snippet?.thumbnails?.standard?.url!
      });
    } else {
      const res = await youtube.search.list({
        part: ['id', 'snippet'],
        q: query
      });

      if (!res.data.items?.length) {
        return message.say('Cannot find any videos matching that query');
      }

      guild.play(message, {
        name: res.data.items[0].snippet?.title!,
        play: `https://www.youtube.com/watch?v=${res.data.items[0].id!.videoId}`,
        thumbnail: res.data.items[0].snippet?.thumbnails?.standard?.url!
      });
    }

    return null;
  }
};