import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';
import {AudioGuild} from '../../util/AudioGuild';

module.exports = class JoinCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      clientPermissions: ['CONNECT'],
      description: 'Join the current voice channel',
      group: 'audio',
      guildOnly: true,
      memberName: 'join',
      name: 'join'
    });
  }

  public async run(message: CommandoMessage) {
    if (!message.member?.voice.channel) {
      return message.say('Join a voice channel first');
    }

    await (message.guild as AudioGuild).connect(message.member.voice.channel);

    return null;
  }
};