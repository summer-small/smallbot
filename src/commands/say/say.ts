import {Command, CommandoClient, CommandoMessage} from 'discord.js-commando';

module.exports = class SayCommand extends Command {
  public constructor(client: CommandoClient) {
    super(client, {
      aliases: ['echo', 'speak'],
      args: [
        {
          key: 'text',
          prompt: 'What should I say?',
          type: 'string'
        }
      ],
      description: 'Make me say something!',
      group: 'say',
      memberName: 'say',
      name: 'say'
    });
  }

  public run(message: CommandoMessage, {text}: {text: string}) {
    return message.say(text);
  }
};