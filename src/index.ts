import {Structures} from 'discord.js';
import {CommandoClient} from 'discord.js-commando';
import * as path from 'path';
import {commandPrefix, owner} from './config.json';
import {AudioGuild} from './util/AudioGuild';

Structures.extend('Guild', _guild => {
  return AudioGuild;
});

const client = new CommandoClient({
  commandPrefix,
  owner
});

client.registry
  .registerDefaultTypes()
  .registerGroups([
    ['say', 'Saying Stuff'],
    ['dice', 'Dice Rolling'],
    ['audio', 'Audio Stuff']
  ])
  .registerDefaultGroups()
  .registerDefaultCommands()
  .registerCommandsIn(path.join(__dirname, 'commands'));

client.once('ready', () => {
  client.user?.setActivity('Ready!');
});

client.on('error', console.error);

const loginToken = process.env.bot_token;
client.login(loginToken);