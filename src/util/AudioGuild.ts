import {StreamDispatcher, VoiceChannel, VoiceConnection} from 'discord.js';
import {CommandoClient, CommandoGuild, CommandoMessage} from 'discord.js-commando';
import internal from 'stream';
import ytdl from 'ytdl-core-discord';
import {VolumeOperation} from '../commands/audio/volume';

export interface PlayItem {
  name: string;

  play: string | internal.Readable;

  thumbnail?: string;
}

export class AudioGuild extends CommandoGuild {
  public connection: VoiceConnection | null;

  public currentSong: PlayItem | null;

  public dispatcher: StreamDispatcher | null;

  public isPlaying = false;

  public message: CommandoMessage | null;

  public queue: PlayItem[] = [];

  public volume: number;

  constructor(commandoClient: CommandoClient, data: object) {
    super(commandoClient, data);

    this.connection = null;
    this.currentSong = null;
    this.dispatcher = null;
    this.isPlaying = false;
    this.message = null;
    this.queue = [];
    this.volume = 1;
  }

  public async connect(channel: VoiceChannel) {
    this.connection = await channel.join();
  }

  public next() {
    this.dispatcher?.end();
  }

  public pause() {
    this.isPlaying = false;
    this.dispatcher?.pause();
  }

  public play(message: CommandoMessage, ...songs: PlayItem[]) {
    this.message = message;

    if (!this.connection) {
      console.log('No connection');

      return;
    }

    this.queue.push(...songs);
    if (!this.isPlaying && !this.dispatcher) {
      this.playSong(this.queue[0].play);
    }
  }

  public resume() {
    this.isPlaying = true;
    this.dispatcher?.resume();
  }

  public setVolume(v: number, op: VolumeOperation = 'set'): number {
    const currentVolume = this.volume;
    if (op === 'set') {
      this.volume = v;
    } else if (op === 'add') {
      this.volume = currentVolume + v;
    } else {
      this.volume = currentVolume - v;
    }

    this.volume = Math.max(Math.min(this.volume, 1), 0);
    this.dispatcher?.setVolume(v);

    return this.volume;
  }

  private async playSong(song: string | internal.Readable) {
    if (!this.connection) {
      return;
    }

    let play;
    if (typeof song === 'string') {
      play = await ytdl(song);
    } else {
      play = song;
    }

    this.dispatcher = this.connection.play(play, {type: 'opus'})
      .on('start', () => {
        this.currentSong = {...this.queue[0]};
        this.isPlaying = true;
        this.dispatcher?.setVolume(this.volume);
        this.message?.say(`Playing ${this.queue[0].name}`);

        return this.queue.shift();
      })
      .on('finish', () => {
        if (!this.queue.length) {
          this.currentSong = null;
          this.isPlaying = false;
          this.dispatcher = null;

          return;
        }

        this.playSong(this.queue[0].play);
      })
      .on('error', err => {
        console.error(err);
        this.message?.say('An error has occurred! Can\'t play that song');
      });
  }
}