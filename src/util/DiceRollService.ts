import {OPERATOR_REGEX, ROLL_REGEX} from './regexes';

export interface DiceResult {
  prettyResults: string[];

  rolls: number[];

  total: number;
}

export type ValidOps = '+' | '-' | '*' | '/';

export class DiceRollService {
  public commands: string[] = [];

  public readonly comment: string = '';

  public errors: string[] = [];

  public result: number = 0;

  public readonly rollArg: string;

  public constructor(rollArg: string) {
    const commentIdx = rollArg.indexOf('#');
    let roll: string;
    if (commentIdx >= 0) {
      roll = rollArg.slice(0, commentIdx);
      this.comment = rollArg.slice(commentIdx + 1).trim();
    } else {
      roll = rollArg;
    }

    this.rollArg = roll.toLowerCase().replace(/\s+/g, '');
    this.parseWholeRoll();
    this.validateCommands();
  }

  public roll(): DiceResult {
    const runningResult: DiceResult = {
      prettyResults: [],
      rolls: [],
      total: 0
    };
    let nextCommand: ValidOps = '+';

    for (const command of this.commands) {
      if (command.match(OPERATOR_REGEX)) {
        nextCommand = command as ValidOps;
      }

      const match = command.match(ROLL_REGEX);
      if (!match) {
        continue;
      }

      const rollResult: DiceResult = {
        prettyResults: [],
        rolls: [],
        total: 0
      };

      const isRoll = match[2] === 'd';
      const nDie = parseInt(match[1], 10) || 1;
      const dieSize = parseInt(match[3], 10);
      const keepExtemes = match[4];

      if (!isRoll) {
        rollResult.prettyResults = [dieSize.toString()];
        rollResult.rolls = [dieSize];
        rollResult.total = dieSize;
      } else {
        const rollRes = DiceRollService.rollDice(nDie, dieSize);

        if (keepExtemes) {
          let keepFn: (nToKeep: number, rolls: number[]) => DiceResult;
          if (keepExtemes === 'a') {
            keepFn = DiceRollService.keepHighest;
          } else if (keepExtemes === 'd') {
            keepFn = DiceRollService.keepLowest;
          } else {
            keepFn = match[4][1] === 'l' ? DiceRollService.keepLowest : DiceRollService.keepHighest;
          }

          const keepRes = keepFn(parseInt(match[4][2], 10) || 1, rollRes.rolls);
          rollResult.prettyResults = keepRes.prettyResults;
          rollResult.rolls = keepRes.rolls;
          rollResult.total = keepRes.total;
        } else {
          rollResult.prettyResults = rollRes.prettyResults;
          rollResult.rolls = rollRes.rolls;
          rollResult.total = rollRes.total;
        }
      }

      runningResult.prettyResults.push(...rollResult.prettyResults);
      runningResult.rolls.push(...rollResult.rolls);
      runningResult.total = this.applyOp(runningResult.total, rollResult.total, nextCommand);
    }

    return runningResult;
  }

  public static rollDice(nDie: number, dieSize: number): DiceResult {
    const rolls: number[] = [];
    let total = 0;
    for (let i = 0; i < nDie; i++) {
      const roll = rollSingle(dieSize);
      rolls.push(roll);
      total += roll;
    }

    return {
      rolls,
      prettyResults: rolls.map(x => x.toString()),
      total
    };
  }

  public static keepHighest(nToKeep: number, rolls: number[]) {
    return keepExtreme('highest', nToKeep, rolls);
  }

  public static keepLowest(nToKeep: number, rolls: number[]) {
    return keepExtreme('lowest', nToKeep, rolls);
  }

  private applyOp(a: number, b: number, op: ValidOps) {
    let res: number;
    switch (op) {
      case '+':
        res = a + b;
        break;
      case '-':
        res = a - b;
        break;
      case '*':
        res = a * b;
        break;
      case '/':
        res = a / b;
    }

    return res;
  }

  private parseWholeRoll() {
    let lastOpIdx = 0;

    for (let i = 0; i < this.rollArg.length; i++) {
      const char = this.rollArg.charAt(i);
      if (char.match(OPERATOR_REGEX)) {
        this.commands.push(this.rollArg.substring(lastOpIdx, i));
        this.commands.push(this.rollArg.substr(i, 1));
        lastOpIdx = i + 1;
      }
    }

    this.commands.push(this.rollArg.substr(lastOpIdx));
  }

  private validateCommands() {
    if (this.commands[0].match(OPERATOR_REGEX)) {
      this.errors.push(`Cannot start roll with ${this.commands[0]}`);
    }

    for (const command of this.commands) {
      if (!command.match(OPERATOR_REGEX) && !command.match(ROLL_REGEX)) {
        this.errors.push(`${command} is an invalid roll`);
      }
    }
  }
}

function rollSingle(dieSize: number, includeZero = false) {
  return Math.floor(Math.random() * dieSize) + (includeZero ? 0 : 1);
}

function keepExtreme(highOrLow: 'highest' | 'lowest', nToKeep: number, rolls: number[]): DiceResult {
  const sortedRolls = [...rolls].sort((a: number, b: number) => a - b);
  const keptRolls = highOrLow === 'highest' ? sortedRolls.slice(-nToKeep) : sortedRolls.slice(0, nToKeep);

  const prettyResults = rolls.map(x => x.toString());
  let total = 0;
  for (const roll of keptRolls) {
    total += roll;
    const idx = prettyResults.indexOf(roll.toString());
    if (idx >= 0) {
      prettyResults[idx] = `---${prettyResults[idx]}`;
    }
  }

  return {
    prettyResults: prettyResults.map(x => {
      if (x.match(/^---/)) {
        return x.slice(3);
      }

      return `~~${x}~~`;
    }),
    rolls: keptRolls,
    total
  };
}