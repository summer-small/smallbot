// Audio Regexes
export const VOLUME_REGEX = new RegExp(/^([+\-])?([0-9]+)%?$/);

// Dice Regexes
export const OPERATOR_REGEX = new RegExp(/[+\-*/]/);
export const ROLL_REGEX = new RegExp(/^([0-9]*)(d?)([0-9]+)((k?[hl]?[0-9]+)|[ad])?$/);

// Youtube Regexes
export const YOUTUBE_PLAYLIST_REGEX = new RegExp(/^(?!.*\?.*\bv=)https:\/\/www\.youtube\.com\/.*\?.*\blist=(.*)$/);
export const YOUTUBE_VIDEO_REGEX = new RegExp(/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/(watch\?v=)?([^\?&\s]+).*$/);